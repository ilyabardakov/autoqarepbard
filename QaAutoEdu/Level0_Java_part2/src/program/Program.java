package program;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import calculator.Arithmetic;
import games.Matrix;
import games.Palindrome;

public class Program {
	public static void main(String[] args) {
		
		
		try
		{
			boolean userWantsToQuit = false;
			while (!userWantsToQuit) {
				System.out.println("Enter the number of task you want to check: ");
				System.out.println("1 - Matrix generator");
				System.out.println("2 - Calculator");
				System.out.println("3 - Palindrom");
				System.out.println("0 - Exit");
				
				int task = Integer.parseInt(new BufferedReader(new InputStreamReader(System.in)).readLine());
				
				switch (task) {
				case 1:
					userWantsToQuit = CheckMatrixGenerator();
					break;
				case 2:
					userWantsToQuit = CheckCalculator();
				case 3:
					CheckPalindrome();
				default:
					userWantsToQuit = true;
					break;
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("Can't recognize the number. Please, repeat");
		}
		System.out.println("Bye!");
		
	}
	
	public static boolean CheckMatrixGenerator() throws IOException {
		System.out.println("Provide a size of array to generate: (or -1 to quit)");
		
		int size = Integer.parseInt(new BufferedReader(new InputStreamReader(System.in)).readLine());
		if (size == -1)
			return true;
		
		int[][] matrix = Matrix.CreateMatrix(size);
		Utils.PrintBinaryArray(matrix);
		
		return false;
	}
	
	public static boolean CheckCalculator() throws IOException {
		System.out.println("Which task would you like to chose: ");
		System.out.println("1 - Array multiplication");
		System.out.println("2 - Power");
		System.out.println("3 - Division");
		System.out.println("4 - Square root");
		
		int answer = Integer.parseInt(new BufferedReader(new InputStreamReader(System.in)).readLine());
		
		switch (answer) {
		case 1:
			// 5! Factorial, BTW 
			int[] whatever = new int[] {1, 2, 3, 4, 5};
			System.out.println("Array has multiplied up to: " + Arithmetic.ArrayMultiplication(whatever));
			break;

		case 2:
			System.out.println("Provide the base");
			double base = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
			System.out.println("And the power");
			int power = Integer.parseInt(new BufferedReader(new InputStreamReader(System.in)).readLine());
			System.out.println(base + " pow " + power + " = " + Arithmetic.Powaaaa(base, power));

		case 3:
			System.out.println("Provide the left num");
			double left = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
			System.out.println("And the power");
			double right = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
			System.out.println(left + " / " + right + " = " + Arithmetic.Division(left, right));
		
		case 4:
			System.out.println("Provide the left num");
			double number = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
			System.out.println("Square root of " + number + " = " + Arithmetic.SquareRoot(number));

		default:
			System.out.println("k");
			break;
		}
		return false;
	}

	public static boolean CheckPalindrome() throws IOException {
		String word = new BufferedReader(new InputStreamReader(System.in)).readLine();
		
		boolean isIt = Palindrome.CheckWord(word);
		
		System.out.println(word + (isIt ? " is " : " isn't ") + "a palindrome");
		
		return false;
		
	}
}
