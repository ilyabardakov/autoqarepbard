package calculator;

public class Arithmetic {
	public static int ArrayMultiplication(int[] source) {
		if (source.length == 0)
			return 0;
		int result = 1;
		for (int i = 0; i < source.length; i++) {
			result *= source[i];
		}
		return result;
	}
	
	public static double Powaaaa(double base, int power) {
		return Math.pow(base, power);
	}
	
	public static double Division(double left, double right) {
		return left / right;
	}
	
	public static double SquareRoot(double number) {
		return Math.sqrt(number);
	}
}
