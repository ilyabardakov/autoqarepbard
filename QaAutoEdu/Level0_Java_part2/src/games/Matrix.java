package games;

public class Matrix {
	public static int[][] CreateMatrix(int size) {
		int[][] result = new int[size][size];
		int nextValue = 1;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				result[i][j] = nextValue;
				nextValue++;
				if (nextValue > 9)
					nextValue = 1;
			}
		}
		
		return result;
	}
}
