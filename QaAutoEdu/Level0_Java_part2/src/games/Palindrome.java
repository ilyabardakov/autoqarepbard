package games;

public class Palindrome {
	public static boolean CheckWord(String word) {
		word = word.toLowerCase();
		word = word.replaceAll("\\s", "");
		
		return word.equals(new StringBuilder(word).reverse().toString());
	}
	
	public static boolean CheckPhrase(String phrase) {
		return CheckWord(phrase);
	}
}
