import com.welcome.Hello;

import java.util.Scanner;

/**
 * Created by ilyabardakov on 11/9/2016.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Please type your name");
        Scanner input = new Scanner(System.in);
        String userName = input.nextLine();
        Hello instance = new Hello();
        instance.setupName(userName);
        instance.welcome();
        System.out.println("Hello world");
        instance.byeBye();
    }
}