package com.welcome;
import java.util.Scanner;

/**
 * Created by ilyabardakov on 11/9/2016.
 */
public class Hello {

    private String _name;
    public void setupName(String name){
        _name = name;
    }

    public void welcome()
    {
        System.out.println("Hello, " + _name);
    }


    public void byeBye()
    {
        System.out.println("Bye, " + _name);
    }
}
